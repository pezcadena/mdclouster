import numpy as np  #librerias para el manejo de arrays y vectores JAIME TA MAL
import csv #libreria para el manejo de archivos CSV
from scipy.spatial import distance

#apertura del archivo para mas referencia ver: https://www.pythonforbeginners.com/files/reading-and-writing-files-in-python
with open('glass.csv') as csvfile:

    #por cada linea que se lee, le da una coma final para separar el vector 
    reader = csv.reader(csvfile,delimiter=',')

    '''a la variable se de añade los datos dentro de un array 2d y para agregar y
    hacer la conversion de cada lista (vector) se usa lis(map(int, linea(vector)))
    para mas referencia sobre vectores y arrays leer: https://datascienceplus.com/vectors-and-arrays-in-python/
    parra mas referencia sobre manejo de listas y mapeado: https://www.geeksforgeeks.org/python-map-function/'''
    vectorZoo = np.array([list(map(float, row)) for row in reader])

#como es un array multidimensional
'''print(vectorZoo[0, 1])
print(vectorZoo[0, 2])
print(vectorZoo)'''
print(len(vectorZoo))

#abre el archivo que creo WEKA de los resultados del SimpleKmeans
with open('glassk25.txt','r') as file:
    data = file.readlines()
    
datos = []

i = 0

#separa cada linea del texto
for line in data:
    datos.insert(i, line.split())
    i += 1

clusters_iniciales = []
k = 0
  
#en esta parte guarda los clusters (centroides) iniciales (random)
for i in range(9, len(datos)):

    try:
        j = i
        if datos[i][0] == 'Cluster':
            x = datos[i][2].split(',')
            clusters_iniciales.insert(k, list(map(float, x)))
            k += 1
    except Exception:
        break

#como son vectores independientes se maneja como si fuera una matriz de arreglos
#print(clusters_de_tabla[0][0])
#print(clusters_iniciales)

clusters_de_tabla = []
index = 0
corr = 2 
j += 8
k = j

#en esta parte guarda los clusters (centroides) de la tabla
for i in range(3,len(datos[k-3])):

    aux = []

    for y in range(len(clusters_iniciales[0])):

        aux.append(float(datos[j][corr]))
        j += 1

    clusters_de_tabla.insert(index, aux)
    corr += 1
    index += 1
    j = k

#como son vectores independientes se maneja como si fuera una matriz de arreglos
#print(clusters_de_tabla[0][0])
#print(clusters_de_tabla[24])




# ─── SE DEBE MODIFICAR VALORK DEPENDIENDO DEL NUMERO DE CLUSTERS ────────────────


valork = 25




#variables de los calculos

instanciasdelcluster = []
listadedistancias = []
for k in range(0,valork):
    instanciasdelcluster.insert(k,list())

#crea las listas de las distancias euclidianas de a acuerdo a el cluster mas cercano

for i in range(0,len(vectorZoo)):
    listadedistancias = []
    for j in range(0,valork):
        listadedistancias.insert(j,distance.euclidean(vectorZoo[i,:], clusters_de_tabla[j]))
    instanciasdelcluster.insert(listadedistancias.index(min(listadedistancias)), vectorZoo[i, :])

#se sacan la desviacion estandar de cada cluster
listadesviacion = []

for k in range(0, valork):
    listadesviacion.insert(k,np.nanstd(instanciasdelcluster[k]))

#Se imprimen los resultados

for i in range(0, valork):
    print("Numero de instancias en el cluster: ",i)
    print(len(instanciasdelcluster[i]))

print()
print("Desviaciones estandar por cluster")
print(listadesviacion)
print()
print("Media de las desviaciones")
print(np.mean(listadesviacion))

