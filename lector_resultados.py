import numpy as np

with open('prueba.txt','r') as file:
    data = file.readlines()
    
datos = []

i = 0

for line in data:
    datos.insert(i, line.split())
    i += 1

clusters_iniciales = []
k = 0
  
for i in range(9, len(datos)):

    try:
        j = i
        if datos[i][0] == 'Cluster':
            x = datos[i][2].split(',')
            clusters_iniciales.insert(k, list(map(int, x)))
            k += 1
    except Exception:
        break

clusters_de_tabla = []
index = 0
corr = 2 
j += 8
k = j

for i in range(3,len(datos[k-3])):

    aux = []

    for y in range(len(clusters_iniciales[0])):

        aux.append(int(datos[j][corr]))
        j += 1

    clusters_de_tabla.insert(index, aux)
    corr += 1
    index += 1
    j = k

print(clusters_de_tabla)